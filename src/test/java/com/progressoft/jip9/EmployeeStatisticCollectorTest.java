package com.progressoft.jip9;

import com.progressoft.jip9.employee_statistics_types.*;
import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.entity.Collector;
import com.progressoft.jip9.entity.Statistic;
import com.progressoft.jip9.entity.StatisticsCollector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeStatisticCollectorTest {

    @Test
    public void givenEmployees_whenCollectStatistics_thenStatisticsReturnedAsExpected() {
        List<Employee> cases = prepareCases();
        BigDecimal[] salaryRange= new BigDecimal[]{BigDecimal.valueOf(350),BigDecimal.valueOf(600),BigDecimal.valueOf(1200)};
        StatisticsCollector<Employee, ? extends Statistic> collector =new Collector<>(new BirthPlaceCount<>(),new BirthYearCount<>(),new PositionCount<>(),new SalaryWithinRangeStatistic<>(salaryRange),new ResignationYearCount<>());

        Assertions.assertNotNull(collector, "you should initialize your collector");
        Iterable<? extends Statistic> statistics = collector.collectStatistics(cases);
        Assertions.assertNotNull(statistics, "returned statistics is null");

        Map<String, Object> mapping = new HashMap<>();
        statistics.forEach(s -> mapping.put(s.getKey(), s.getValue()));
        Assertions.assertFalse(mapping.isEmpty(), "no statistics was returned");
        Map<String, Object> expected = new HashMap<>();


        expected.put("born in 1997", 2);
        expected.put("born in 1995", 3);

        expected.put("born in Jordan", 2);
        expected.put("born in Palestine", 1);
        expected.put("born in egypt", 2);

        expected.put("salary < 350", 1);
        expected.put("350 <= salary < 600", 1);
        expected.put("600 <= salary < 1200", 2);
        expected.put("salary >= 1200", 1);

        expected.put("resigned in 2018", 2);
        expected.put("resigned in 2019", 1);

        expected.put("developer", 3);
        expected.put("project manager", 1);
        expected.put("quality control", 1);
        Assertions.assertEquals(expected, mapping, "returned statistics are not within expected");
    }

    private List<Employee> prepareCases() {
        return Arrays.asList(new Employee.Builder()
                        .setFirstName("sami")
                        .setLastName("ahmad")
                        .setBirthDate(LocalDate.of(1995, 3, 1))
                        .setBirthPlace("Jordan")
                        .setSalary(BigDecimal.valueOf(340))
                        .setResignationDate(LocalDate.of(2018, 10, 10))
                        .setPosition("developer")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Moustafa")
                        .setLastName("Ismail")
                        .setBirthDate(LocalDate.of(1995, 4, 11))
                        .setBirthPlace("Palestine")
                        .setSalary(BigDecimal.valueOf(400))
                        .setPosition("developer")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Abdellrahma")
                        .setLastName("Saed")
                        .setBirthDate(LocalDate.of(1995, 5, 1))
                        .setBirthPlace("Jordan")
                        .setSalary(BigDecimal.valueOf(700))
                        .setResignationDate(LocalDate.of(2018, 11, 10))
                        .setPosition("developer")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Hussam")
                        .setLastName("Husni")
                        .setBirthDate(LocalDate.of(1997, 5, 1))
                        .setBirthPlace("egypt")
                        .setSalary(BigDecimal.valueOf(1_000))
                        .setPosition("quality control")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Mohammad")
                        .setLastName("Abdelsalam")
                        .setBirthDate(LocalDate.of(1997, 10, 15))
                        .setSalary(BigDecimal.valueOf(1_430))
                        .setBirthPlace("egypt")
                        .setResignationDate(LocalDate.of(2019, 10, 10))
                        .setPosition("project manager")
                        .build());

    }
}
