package com.progressoft.jip9;

import com.progressoft.jip9.entity.Collector;
import com.progressoft.jip9.entity.Statistic;
import com.progressoft.jip9.entity.StatisticsCollector;
import com.progressoft.jip9.string_statistics_types.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringStatisticCollectorTest {

    @Test
    public void givenStringCases_whenCollectStatistics_thenStatisticsReturnedAsExpected() {
        List<String> cases = prepareCases();
        StatisticsCollector<String, ? extends Statistic> collector = new Collector<>(new DigitsCount<>(),new LowerCaseCount<>(),new UpperLettersCount<>(),new WordCount<>(),new NonLetterCount<>());
        Assertions.assertNotNull(collector, "you should initialize your collector");
        Iterable<? extends Statistic> statistics = collector.collectStatistics(cases);
        Assertions.assertNotNull(statistics, "returned statistics is null");

        Map<String, Object> mapping = new HashMap<>();
        statistics.forEach(s -> mapping.put(s.getKey(), s.getValue()));
        Assertions.assertFalse(mapping.isEmpty(), "no statistics was returned");
        Map<String, Object> expected = new HashMap<>();
        expected.put("upper case", 3);
        expected.put("lower case", 37);
        expected.put("non-word", 4);
        expected.put("total words", 8);
        expected.put("digits", 3);
        Assertions.assertEquals(expected, mapping, "returned statistics are not within expected");
    }

    private List<String> prepareCases() {
        return Arrays.asList("hello world",
                "Hakona Matata!",
                "my password P@ssw0rd",
                "23%&kl");
    }
}
