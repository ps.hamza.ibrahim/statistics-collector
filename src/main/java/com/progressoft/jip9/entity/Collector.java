package com.progressoft.jip9.entity;


import java.util.*;

public class Collector<T,S extends Statistic>implements StatisticsCollector<T,S> {
    private final Statistics<T>[] statistics;

    @SafeVarargs
    public Collector(Statistics<T>...statistics) {
        this.statistics=statistics;
    }

    @Override
    public Iterable<S> collectStatistics(Iterable<T> objects) {
        List<S>result = new ArrayList<>();
        objects.forEach(
                obj->{
                    failIfNull(obj);
                    for (Statistics<T> statistic : statistics) {
                        statistic.collect(obj);
                    }
                }
        );
        Map<String,Integer> returnedStatistics;
        for (Statistics<T> statistic : statistics) {
            returnedStatistics=statistic.getStatistic();
            returnedStatistics.forEach((key,value)->result.add((S) new StatisticImpl(key,value)));
        }
        return result;
    }

    private void failIfNull(T emp) {
        if (emp==null)
            throw new IllegalArgumentException("employee should not be null");
    }

}
