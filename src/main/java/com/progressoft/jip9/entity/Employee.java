package com.progressoft.jip9.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Employee {
    private final String firstName;
    private final String lastName;
    private final String birthPlace;
    private final String position;
    private final BigDecimal salary;
    private final LocalDate birthDate;
    private final LocalDate hiringDate;
    private final LocalDate resignationDate;

    private Employee(String firstName,
                     String lastName,
                     String birthPlace,
                     String position,
                     BigDecimal salary,
                     LocalDate birthDate,
                     LocalDate hiringDate,
                     LocalDate resignationDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthPlace = birthPlace;
        this.position = position;
        this.salary = salary;
        this.birthDate = birthDate;
        this.hiringDate = hiringDate;
        this.resignationDate = resignationDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public LocalDate getHiringDate() {
        return hiringDate;
    }

    public LocalDate getResignationDate() {
        return resignationDate;
    }

    public String getPosition() {
        return position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                ", birthDate=" + birthDate +
                ", hiringDate=" + hiringDate +
                ", resignationDate=" + resignationDate +
                '}';
    }

    public static class Builder {
        private  String firstName;
        private  String lastName;
        private  String birthPlace;
        private  String position;
        private  BigDecimal salary;
        private  LocalDate birthDate;
        private  LocalDate hiringDate;
        private  LocalDate resignationDate;



        public Builder setFirstName(String firstName) {
            failIfNull(firstName);
            this.firstName=firstName;
            return this;
        }

        private void failIfNull(Object value) {
            if (value == null )
                throw new IllegalArgumentException("the property shouldn't be null ");
        }

        public Builder setLastName(String lastName) {
            failIfNull(lastName);
            this.lastName = lastName;
            return this;
        }

        public Builder setBirthPlace(String birthPlace) {
            failIfNull(birthPlace);
            this.birthPlace = birthPlace;
            return this;
        }

        public Builder setBirthDate(LocalDate birthDate) {
            failIfNull(birthDate);
            this.birthDate = birthDate;
            return this;
        }

        public Builder setHiringDate(LocalDate hiringDate) {
            failIfNull(hiringDate);
            this.hiringDate = hiringDate;
            return this;
        }

        public Builder setResignationDate(LocalDate resignationDate) {
            this.resignationDate = resignationDate;
            return this;
        }

        public Builder setPosition(String position) {
            failIfNull(position);
            this.position = position;
            return this;
        }

        public Builder setSalary(BigDecimal salary) {
            failIfNull(salary);
            this.salary = salary;
            return this;
        }

        public Employee build() {
            return new Employee(firstName,lastName,birthPlace,position,salary,birthDate,hiringDate,resignationDate);
        }
    }
}
