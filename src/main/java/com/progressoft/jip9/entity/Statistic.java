package com.progressoft.jip9.entity;

/**
* Defines a single statistical information, like the number of
* people according to birth year, number of employees according to birth place, the total of prime numbers from an array.
* etc...
* <p>
* Each Statistic instance has a unique identifier {@link #getKey()} and the
* calculated value {@link #getValue()}, for example: a statistic for number of
* peoples aggregated by birth year shall return the year as a key and the
* number of births as a value.
*/
public interface Statistic {
/**
* The key of this statistic
* 
* @return statistic key
*/
     String getKey();
/**
* The aggregated value of this statistic, returned value shall have proper implementation for toString
* 
* @return aggregated value
*/
     Object getValue();

}