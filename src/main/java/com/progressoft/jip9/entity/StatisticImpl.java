package com.progressoft.jip9.entity;


public class StatisticImpl implements Statistic {
    private final String key;
    private final Integer value;

    public StatisticImpl(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
