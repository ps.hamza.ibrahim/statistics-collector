package com.progressoft.jip9.entity;

import com.progressoft.jip9.entity.Employee;

import java.util.Map;

public interface Statistics<T> {
    void collect(T t);
    Map<String,Integer> getStatistic();
}
