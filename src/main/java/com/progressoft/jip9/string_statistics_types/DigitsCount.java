package com.progressoft.jip9.string_statistics_types;

import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class DigitsCount<T extends String>implements Statistics<T> {
    private final Map<String,Integer>statisticCount;

    public DigitsCount() {
        statisticCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        for(int i=0;i<t.length();i++){
            if(t.charAt(i)>='0'&&t.charAt(i)<='9'){
                int count = statisticCount.getOrDefault("digits",0);
                statisticCount.put("digits",count+1);
            }
        }
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticCount;
    }
}
