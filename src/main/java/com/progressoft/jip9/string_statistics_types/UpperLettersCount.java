package com.progressoft.jip9.string_statistics_types;

import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class UpperLettersCount<T extends String> implements Statistics<T> {
    private final Map<String,Integer> statisticsCount;


    public UpperLettersCount() {
        statisticsCount =new HashMap<>();

    }

    @Override
    public void collect(T t) {
        for(int i = 0; i< t.length(); i++){
            int count;
            if (t.charAt(i)>='A' && t.charAt(i)<='Z') {
                count = statisticsCount.getOrDefault("upper case", 0);
                statisticsCount.put("upper case",count+1);
            }
        }
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticsCount;
    }


}
