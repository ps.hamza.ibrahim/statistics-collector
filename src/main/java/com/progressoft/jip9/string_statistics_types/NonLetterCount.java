package com.progressoft.jip9.string_statistics_types;

import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class NonLetterCount<T extends String> implements Statistics<T> {
    private final Map <String,Integer>statisticsCount;

    public NonLetterCount() {
        statisticsCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        Pattern pattern =Pattern.compile(".*[`~!@#$%^&*()\\-_=+\\\\|\\[{\\]};:'\",<.>/?].*");
        for (int i = 0; i < t.length(); i++) {
            if (pattern.matcher(String.valueOf(t.charAt(i))).matches()) {
                Integer count = statisticsCount.getOrDefault("non-word", 0);
                statisticsCount.put("non-word", count + 1);
            }
        }
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticsCount;
    }
}
