package com.progressoft.jip9.string_statistics_types;

import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class WordCount <T extends String> implements Statistics<T> {
    private final Map<String,Integer>statisticsCount;

    public WordCount() {
        statisticsCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        String[] strings = t.split(" ");
        int count = statisticsCount.getOrDefault("total words", 0);
        statisticsCount.put("total words",count+strings.length);
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticsCount;
    }
}
