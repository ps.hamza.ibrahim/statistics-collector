package com.progressoft.jip9.string_statistics_types;

import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class LowerCaseCount<T extends String> implements Statistics<T> {
    private final Map<String,Integer>statisticsCount;

    public LowerCaseCount() {
        statisticsCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        for(int i = 0; i< t.length(); i++){
            if (t.charAt(i)>='a' && t.charAt(i)<='z') {
                Integer count = statisticsCount.getOrDefault("lower case", 0);
                statisticsCount.put("lower case",count+1);
            }
        }
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticsCount;
    }
}
