package com.progressoft.jip9.employee_statistics_types;

import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class BirthYearCount<T extends Employee> implements Statistics<T> {
    private final Map<String,Integer>statisticsCount;

    public BirthYearCount() {
        statisticsCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        int count = statisticsCount.getOrDefault("born in "+ t.getBirthDate().getYear(),0);
        statisticsCount.put("born in "+ t.getBirthDate().getYear(),count+1);
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticsCount;
    }
}
