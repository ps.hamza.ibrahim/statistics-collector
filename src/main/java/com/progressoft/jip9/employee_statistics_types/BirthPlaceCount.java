package com.progressoft.jip9.employee_statistics_types;

import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class BirthPlaceCount<T extends Employee> implements Statistics<T> {
    private  final Map<String,Integer>statisticCount;

    public BirthPlaceCount() {
        this.statisticCount = new HashMap<>();
    }

    @Override
    public void collect(T t) {
        int count = statisticCount.getOrDefault("born in "+ t.getBirthPlace(),0);
        statisticCount.put("born in "+ t.getBirthPlace(),count+1);
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticCount;
    }
}
