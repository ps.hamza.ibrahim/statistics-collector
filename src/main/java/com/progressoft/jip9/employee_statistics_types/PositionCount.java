package com.progressoft.jip9.employee_statistics_types;

import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class PositionCount<T extends Employee> implements Statistics<T> {
    private final HashMap<String,Integer>statisticCount;

    public PositionCount() {
        statisticCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        int count = statisticCount.getOrDefault(t.getPosition(),0);
        statisticCount.put(t.getPosition(), count+1);
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticCount;
    }
}
