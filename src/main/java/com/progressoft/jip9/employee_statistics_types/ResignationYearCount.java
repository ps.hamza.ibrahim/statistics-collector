package com.progressoft.jip9.employee_statistics_types;

import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.entity.Statistics;

import java.util.HashMap;
import java.util.Map;

public class ResignationYearCount<T extends Employee> implements Statistics<T> {
    private final HashMap<String,Integer>statisticCount;

    public ResignationYearCount() {
        statisticCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        if (t.getResignationDate()==null)
            return;
        int count = statisticCount.getOrDefault("resigned in "+ t.getResignationDate().getYear(),0);
        statisticCount.put("resigned in "+ t.getResignationDate().getYear(),count+1);
    }

    @Override
    public Map<String, Integer> getStatistic() {
        return statisticCount;
    }
}
