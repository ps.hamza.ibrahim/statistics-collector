package com.progressoft.jip9.employee_statistics_types;

import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.entity.Statistics;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class SalaryWithinRangeStatistic<T extends Employee> implements Statistics<T> {
    private final BigDecimal[] range;
    private final Map<String,Integer>statisticCount;

    public SalaryWithinRangeStatistic(BigDecimal[] range) {
        this.range = range;
        statisticCount=new HashMap<>();
    }

    @Override
    public void collect(T t) {
        if (t.getSalary().compareTo(range[0])<0) {
            statisticCount.put("salary < "+range[0], statisticCount.getOrDefault("salary < "+range[0], 1));
            return;
        }
        for(int i=0;i< range.length-1;i++){
            if (t.getSalary().compareTo(range[i])>=0 && t.getSalary().compareTo(range[i+1])<0) {
                Integer count = statisticCount.getOrDefault(range[i] + " <= salary < " + range[i + 1], 0);
                statisticCount.put(range[i] + " <= salary < " + range[i + 1], count+1);
                return;
            }
        }
        statisticCount.put("salary >= "+range[range.length-1], statisticCount.getOrDefault("salary >= "+range[range.length-1],1));
    }

    @Override
    public Map<String,Integer> getStatistic() {
        return statisticCount;
    }
}
